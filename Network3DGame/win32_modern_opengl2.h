// https://www.opengl.org/registry/api/GL/glext.h

#define GL_MAJOR_VERSION                  0x821B
#define GL_MINOR_VERSION                  0x821C

#define GL_ARRAY_BUFFER                   0x8892
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_STATIC_DRAW                    0x88E4

#define GL_FRAGMENT_SHADER                0x8B30
#define GL_VERTEX_SHADER                  0x8B31

#define GL_COMPILE_STATUS                 0x8B81
#define GL_LINK_STATUS                    0x8B82
#define GL_VALIDATE_STATUS                0x8B83
#define GL_INFO_LOG_LENGTH                0x8B84


#include <stddef.h>
typedef ptrdiff_t GLsizeiptr;
typedef ptrdiff_t GLintptr;
typedef char GLchar;


// typedef all function pointers.
// function name can be anything, I use gl_* as it's a small change when copy-pasting.
typedef GLuint WINAPI gl_CreateProgram(void);
typedef GLuint WINAPI gl_CreateShader(GLenum shaderType);
typedef void WINAPI gl_ShaderSource(GLuint shader, GLsizei count, const GLbyte ** string, const GLint * length);
typedef void WINAPI gl_CompileShader(GLuint shader);
typedef void WINAPI gl_AttachShader(GLuint program, GLuint shader);
typedef void WINAPI gl_LinkProgram(GLuint program);
typedef void WINAPI gl_ValidateProgram(GLuint program);
typedef void WINAPI gl_UseProgram(GLuint program);
typedef void WINAPI gl_GetShaderInfoLog(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
typedef void WINAPI gl_GetShaderiv(GLuint shader, GLenum pname, GLint * params);
typedef void WINAPI gl_GetProgramInfoLog(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog);
typedef void WINAPI gl_GetProgramiv(GLuint program, GLenum pname, GLint *params);
typedef GLint WINAPI gl_GetUniformLocation(GLuint program, const GLchar *name);
typedef void WINAPI gl_Uniform1f(GLint location, GLfloat v0);
typedef void WINAPI gl_UniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void WINAPI gl_GenVertexArrays(GLsizei n, GLuint *arrays);
typedef void WINAPI gl_BindVertexArray(GLuint array);
typedef GLint WINAPI gl_GetAttribLocation(GLuint program, const GLchar * name);
typedef void WINAPI gl_VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer);
typedef void WINAPI gl_VertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid * pointer);
typedef void WINAPI gl_GenBuffers(GLsizei n, GLuint * buffers);
typedef void WINAPI gl_BindBuffer(GLenum target, GLuint buffer);
typedef void WINAPI gl_BufferData(GLenum target, GLsizeiptr size, const GLvoid * data, GLenum usage);
typedef void WINAPI gl_BufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid * data);
typedef void WINAPI gl_EnableVertexAttribArray(GLuint index);
typedef void WINAPI gl_DisableVertexAttribArray(GLuint index);

// allocate function pointers
static gl_CreateProgram* glCreateProgram;
static gl_CreateShader* glCreateShader;
static gl_ShaderSource* glShaderSource;
static gl_CompileShader* glCompileShader;
static gl_AttachShader* glAttachShader;
static gl_LinkProgram* glLinkProgram;
static gl_ValidateProgram* glValidateProgram;
static gl_UseProgram* glUseProgram;
static gl_GetShaderInfoLog* glGetShaderInfoLog;
static gl_GetShaderiv* glGetShaderiv;
static gl_GetProgramInfoLog* glGetProgramInfoLog;
static gl_GetProgramiv* glGetProgramiv;
static gl_GetUniformLocation* glGetUniformLocation;
static gl_Uniform1f* glUniform1f;
static gl_UniformMatrix4fv* glUniformMatrix4fv;
static gl_GenVertexArrays* glGenVertexArrays;
static gl_BindVertexArray* glBindVertexArray;
static gl_GetAttribLocation* glGetAttribLocation;
static gl_VertexAttribPointer* glVertexAttribPointer;
static gl_VertexAttribIPointer* glVertexAttribIPointer;
static gl_GenBuffers* glGenBuffers;
static gl_BindBuffer* glBindBuffer;
static gl_BufferData* glBufferData;
static gl_BufferSubData* glBufferSubData;
static gl_EnableVertexAttribArray* glEnableVertexAttribArray;
static gl_DisableVertexAttribArray* glDisableVertexAttribArray;


// load function pointers into their allocated variables
// if any function is missing from computers opengl drivers return 0, else 1
static int Win32LoadOpenGLFunctions(void) {
 	if(!(glCreateProgram = (gl_CreateProgram*)wglGetProcAddress("glCreateProgram"))) return 0;
	if(!(glCreateShader = (gl_CreateShader*)wglGetProcAddress("glCreateShader"))) return 0;
	if(!(glShaderSource = (gl_ShaderSource*)wglGetProcAddress("glShaderSource"))) return 0;
	if(!(glCompileShader = (gl_CompileShader*)wglGetProcAddress("glCompileShader"))) return 0;
	if(!(glAttachShader = (gl_AttachShader*)wglGetProcAddress("glAttachShader"))) return 0;
	if(!(glLinkProgram = (gl_LinkProgram*)wglGetProcAddress("glLinkProgram"))) return 0;
	if(!(glValidateProgram = (gl_ValidateProgram*)wglGetProcAddress("glValidateProgram"))) return 0;
	if(!(glUseProgram = (gl_UseProgram*)wglGetProcAddress("glUseProgram"))) return 0;
	if(!(glGetShaderInfoLog = (gl_GetShaderInfoLog*)wglGetProcAddress("glGetShaderInfoLog"))) return 0;
	if(!(glGetShaderiv = (gl_GetShaderiv*)wglGetProcAddress("glGetShaderiv"))) return 0;
	if(!(glGetProgramInfoLog = (gl_GetProgramInfoLog*)wglGetProcAddress("glGetProgramInfoLog"))) return 0;
	if(!(glGetProgramiv = (gl_GetProgramiv*)wglGetProcAddress("glGetProgramiv"))) return 0;
	if(!(glGetUniformLocation = (gl_GetUniformLocation*)wglGetProcAddress("glGetUniformLocation"))) return 0;
	if(!(glUniform1f = (gl_Uniform1f*)wglGetProcAddress("glUniform1f"))) return 0;
	if(!(glUniformMatrix4fv = (gl_UniformMatrix4fv*)wglGetProcAddress("glUniformMatrix4fv"))) return 0;
	if(!(glGenVertexArrays = (gl_GenVertexArrays*)wglGetProcAddress("glGenVertexArrays"))) return 0;
	if(!(glBindVertexArray = (gl_BindVertexArray*)wglGetProcAddress("glBindVertexArray"))) return 0;
	if(!(glGetAttribLocation = (gl_GetAttribLocation*)wglGetProcAddress("glGetAttribLocation"))) return 0;
	if(!(glVertexAttribPointer = (gl_VertexAttribPointer*)wglGetProcAddress("glVertexAttribPointer"))) return 0;
	if(!(glVertexAttribIPointer = (gl_VertexAttribIPointer*)wglGetProcAddress("glVertexAttribIPointer"))) return 0;
	if(!(glGenBuffers = (gl_GenBuffers*)wglGetProcAddress("glGenBuffers"))) return 0;
	if(!(glBindBuffer = (gl_BindBuffer*)wglGetProcAddress("glBindBuffer"))) return 0;
	if(!(glBufferData = (gl_BufferData*)wglGetProcAddress("glBufferData"))) return 0;
	if(!(glBufferSubData = (gl_BufferSubData*)wglGetProcAddress("glBufferSubData"))) return 0;
	if(!(glEnableVertexAttribArray = (gl_EnableVertexAttribArray*)wglGetProcAddress("glEnableVertexAttribArray"))) return 0;
	if(!(glDisableVertexAttribArray = (gl_DisableVertexAttribArray*)wglGetProcAddress("glDisableVertexAttribArray"))) return 0;

	return 1;
}

static GLint Win32CheckShaderCompileStatus(GLuint shader){
	GLint compileStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	if(compileStatus == GL_FALSE){
		GLint infoLogLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength > 0){
			GLsizei charsWritten;
			char* infoLog = (char*)malloc(infoLogLength);
			glGetShaderInfoLog(shader, infoLogLength, &charsWritten, infoLog);
			wchar_t* wideString = (wchar_t*)malloc(infoLogLength*sizeof(wchar_t));
			MultiByteToWideChar(CP_ACP, 0, infoLog, -1, wideString, infoLogLength);
			MessageBox(NULL, wideString, L"Error", MB_OK);
			free(infoLog);
			free(wideString);
		}
	}
	return compileStatus;
}

static GLint Win32CheckProgramLinkStatus(GLuint program){
  GLint linkStatus;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if(linkStatus == GL_FALSE){
		GLint infoLogLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength > 0){
			GLsizei charsWritten;
			char* infoLog = (char*)malloc(infoLogLength);
			glGetProgramInfoLog(program, infoLogLength, &charsWritten, infoLog);
			wchar_t* wideString = (wchar_t*)malloc(infoLogLength*sizeof(wchar_t));
			MultiByteToWideChar(CP_ACP, 0, infoLog, -1, wideString, infoLogLength);
			MessageBox(NULL, wideString, L"Error", MB_OK);
			free(infoLog);
			free(wideString);
		}
	}
	return linkStatus;
}

static GLint Win32CheckProgramValidateStatus(GLuint program){
  GLint validateStatus;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &validateStatus);
	if(validateStatus == GL_FALSE){
		GLint infoLogLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength > 0){
			GLsizei charsWritten;
			char* infoLog = (char*)malloc(infoLogLength);
			glGetProgramInfoLog(program, infoLogLength, &charsWritten, infoLog);
			wchar_t* wideString = (wchar_t*)malloc(infoLogLength*sizeof(wchar_t));
			MultiByteToWideChar(CP_ACP, 0, infoLog, -1, wideString, infoLogLength);
			MessageBox(NULL, wideString, L"Error", MB_OK);
			free(infoLog);
			free(wideString);
		}
	}
	return validateStatus;
}

