/*
VSH

uniform vec3 lightDir
uniform mat4 modelview;
uniform mat4 projection;

in vec3 vPos;
in vec3 vNormal;

out vec3 color

main(){
vec3 normal = modelview * Vec4(vnormal,0);
vec3 invLightDir = mat3(modelview) * lightdir;

vormal = normalize(normal
invLightDir = normalize(invLightDir);

float intensity = dot(normal, invLightDir);

//vec3 diffuse = intensity * vec3(0.8,0.4,0.4)


color = intensity * ve3(1,0,0)

}

*/
#define UNICODE
#define _UNICODE
#define _USE_MATH_DEFINES

#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <gl/gl.h>
#include <gl/GLU.h>
#include <math.h>
#include <stdio.h>
#include<time.h>
#include <iostream>
//#include "vld.h"
#include "vec3.h"
#include "Lists.h"
#include "win32_modern_opengl2.h"
#include "mat4.h"
#include "timer.h"
#include "network_protocol.h"


#pragma comment (lib, "Ws2_32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#define DEFAULT_PORT "27020"
#define BUFSIZE 512

SOCKET connectSocket;
addrinfo result;
ClientToServerMessage ctsmsg;
ServerToClientMessage stcmsg;
char recvbuff[512];
bool connected = false;
int clientcurrentframe = 0;



static int width = 600;
static int height = 400;
int isRunning = 0;

float boop = 0;
float boop2 = 0;
float circlemove = 0;

float yeah = 0;
bool yeahswitch = true;


char networkID = -1;
GameState currentgamestate;
GameState lastgamestate;
vec3 player1pos = Vec3(0, 0, 0);

static GLuint shaderProgram;
GLuint vbo;
GLuint ebo;
GLuint hmebo;
GLuint bulletebo;
const char* fshSource = R"asdasd(
#version 330

in vec3 color;

out vec4 fragColor;
out float gl_FragDepth;

void main(){
	fragColor = vec4(color, 1);
	
}
)asdasd";

const char* fshSource2 = R"asdasd(
#version 330

uniform mat4 inMat;

in vec3 vertexPosition;
in vec3 vertexColor;

out vec3 color;

void main(){
	color = vertexColor;

		gl_Position =  inMat * vec4(vertexPosition, 1.0);
}
)asdasd";

/*

mat4 proj = transpose(mat4(1.5, 0.0, 0.0, 0.0,
0.0, 1.5, 0.0, 0.0,
0.0, 0.0, -1.001501126, -3.002251689,
0.0, 0.0, -1.0, 0.0));

mat4 modelview = transpose(mat4(1,0,0,0, 0,1,0,0, 0,0,1,-3, 0,0,0,1));

vec3 modifypos = vec3(vertexPosition.x, vertexPosition.y, vertexPosition.z + fooPos);
gl_Position = proj * modelview * vec4(modifypos, 1.0);
*/
void createhmColor()
{
	for (int i = 2; i <= sizeof(hmVertices) / sizeof(hmVertices[0]); i += 3)
	{
		if (rand() % 101 > 50)
		{
		hmVertices[i] = (float)rand() / (float)RAND_MAX;
			if (hmVertices[i] >= 0.6f)
			{
				hmVertices[i] = 0.56f;
			}
		}

	}
	for (int i = 2; i <= sizeof(hmVertices) / sizeof(hmVertices[0]); i += 3)
	{
		if (hmVertices[i] > 0)
		{
			hmColors[i - 2] = 1.f * hmVertices[i];
			hmColors[i - 1] = 0.f;
			hmColors[i] = 0.f;
		}
		else if (hmVertices[i] < 0)
		{
			hmColors[i - 2] = 0.f;
			hmColors[i - 1] = 0.f;
			hmColors[i] = 1.f;
		}
		else
		{
			hmColors[i - 2] = 0.f;
			hmColors[i - 1] = 1.f;
			hmColors[i] = 0.f;
		}
	}
}

static void initOpenGL() {

	createhmColor();
	hmIndices[0];
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(hmVertices) + sizeof(hmColors) /*+ sizeof(bulletVertices) + sizeof(bulletColors)*/, 0, GL_STATIC_DRAW);
	
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), colors);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors), sizeof(hmVertices), hmVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(hmVertices), sizeof(hmColors), hmColors);
	/*glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(hmVertices) + sizeof(hmColors), sizeof(bulletVertices), bulletVertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors) + sizeof(hmVertices) + sizeof(hmColors) + sizeof(bulletVertices), sizeof(bulletColors), bulletColors);
*/
	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glGenBuffers(1, &hmebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, hmebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(hmIndices), hmIndices, GL_STATIC_DRAW);
	/*glGenBuffers(1, &bulletebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bulletebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(bulletIndices), bulletIndices, GL_STATIC_DRAW);*/

	GLuint fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint vertexshader = glCreateShader(GL_VERTEX_SHADER);
	if (fragmentshader && vertexshader)
	{
		glShaderSource(fragmentshader, 1, (const GLbyte**)&fshSource, 0);
		glCompileShader(fragmentshader);

		glShaderSource(vertexshader, 1, (const GLbyte**)&fshSource2, 0);
		glCompileShader(vertexshader);

		if (Win32CheckShaderCompileStatus(fragmentshader))
		{
			shaderProgram = glCreateProgram();
			if (shaderProgram)
			{
				glAttachShader(shaderProgram, fragmentshader);
				glAttachShader(shaderProgram, vertexshader);

				glLinkProgram(shaderProgram);
				Win32CheckProgramLinkStatus(shaderProgram);

				glValidateProgram(shaderProgram);
				Win32CheckProgramValidateStatus(shaderProgram);

				glUseProgram(shaderProgram);

			}
		}

	}
}

void draw() {

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);

	LARGE_INTEGER Result;
	QueryPerformanceCounter(&Result);
	glClearColor(0.6f, 0.0f, 0.2f, 1);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	boop += 0.001f;
	boop2 += 0.001f;
	glUseProgram(shaderProgram);

	//vertex shader model in stuff
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	GLint posAttrib = glGetAttribLocation(shaderProgram, "vertexPosition");
	GLint colorAttrib = glGetAttribLocation(shaderProgram, "vertexColor");
	//TODO: Make models based on network players
	//Heightmap

	mat4 m = createFrustumMatrix(-1, 1, -1, 1, 30, 30000);

//if (yeahswitch)
//	{
//		yeah += 0.1f;
//		if (yeah >= 100.f)
//		{
//			yeahswitch = false;
//		}
//	}
//	if (!yeahswitch)
//	{
//		yeah -= 0.1f;
//		if (yeah <= -100.f)
//		{
//			yeahswitch = true;
//		}
//	}
	vec3 mypos = currentgamestate.players[0].position;
	
	//playerpos = Vec3( cos(circlemove)*2, sin(circlemove)*2, 0.2f);
	m = lookAt(&m, 0.f, 0.f,200.f,0.f, 0.f,0.f, 0.f, -1.f, 0.f);
	
	GLuint inm = glGetUniformLocation(shaderProgram, "inMat");
	//Cars//
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(colorAttrib);
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices)));
	if (currentgamestate.players[0].ID != -1)
	{
		mat4 matrix1 = m;
		translate(&matrix1, currentgamestate.players[0].position.x, currentgamestate.players[0].position.y, currentgamestate.players[0].position.z);
		rotate(&matrix1, currentgamestate.players[0].rotation, 0, 0, 1);
		glUniformMatrix4fv(inm, 1, GL_FALSE, matrix1.m);
		glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
	}

	if (currentgamestate.players[1].ID != -1)
	{
		mat4 matrix2 = m;
		translate(&matrix2, currentgamestate.players[1].position.x, currentgamestate.players[1].position.y, currentgamestate.players[1].position.z);
		rotate(&matrix2, currentgamestate.players[1].rotation, 0, 0, 1);
		glUniformMatrix4fv(inm, 1, GL_FALSE, matrix2.m);
		glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
	}

	//bullets
	if (currentgamestate.players[0].isBulletActive)
	{
		mat4 matrix3 = m;
		translate(&matrix3, currentgamestate.players[0].bulletPosition.x, currentgamestate.players[0].bulletPosition.y, currentgamestate.players[0].bulletPosition.z);
		rotate(&matrix3, currentgamestate.players[0].bulletrotation, 0, 0, 1);
		scale(&matrix3, 0.25f, 0.25f, 0.25f);
		glUniformMatrix4fv(inm, 1, GL_FALSE, matrix3.m);
		glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
	}
	if (currentgamestate.players[1].isBulletActive)
	{
		mat4 matrix4 = m;
		translate(&matrix4, currentgamestate.players[1].bulletPosition.x, currentgamestate.players[1].bulletPosition.y, currentgamestate.players[1].bulletPosition.z);
		rotate(&matrix4, currentgamestate.players[1].bulletrotation, 0, 0, 1);
		scale(&matrix4, 0.25f, 0.25f, 0.25f);
		glUniformMatrix4fv(inm, 1, GL_FALSE, matrix4.m);
		glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(indices[0]), GL_UNSIGNED_INT, 0);
	}


	//Heightmap//
	mat4 hmMatrix = m;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, hmebo);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors)));
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors) + sizeof(hmVertices)));
	translate(&hmMatrix, 0.f, 0.f, 0.f);
	scale(&hmMatrix, 2, 2, 1);
	glUniformMatrix4fv(inm, 1, GL_FALSE, hmMatrix.m);
	glDrawElements(GL_TRIANGLES, sizeof(hmIndices) / sizeof(hmIndices[0]), GL_UNSIGNED_INT, 0);

	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	/*mat4 bulletMatrix1 = m;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bulletebo);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors) + sizeof(hmVertices) + sizeof(hmColors)));
	glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(vertices) + sizeof(colors) + sizeof(hmVertices) + sizeof(hmColors) + sizeof(bulletVertices)));

	translate(&matrix1, currentgamestate.players[0].bulletPosition.x, currentgamestate.players[0].bulletPosition.y, currentgamestate.players[0].bulletPosition.z);
	rotate(&matrix1, currentgamestate.players[0].bulletrotation, 0, 0, 1);
	glUniformMatrix4fv(inm, 1, GL_FALSE, bulletMatrix1.m);
	glDrawElements(GL_TRIANGLES, sizeof(bulletIndices) / sizeof(bulletIndices[0]), GL_UNSIGNED_INT, 0);

	mat4 bulletMatrix2 = m;
	translate(&matrix1, currentgamestate.players[0].bulletPosition.x, currentgamestate.players[0].bulletPosition.y, currentgamestate.players[0].bulletPosition.z);
	rotate(&matrix1, currentgamestate.players[0].bulletrotation, 0, 0, 1);
	glUniformMatrix4fv(inm, 1, GL_FALSE, bulletMatrix2.m);
	glDrawElements(GL_TRIANGLES, sizeof(bulletIndices) / sizeof(bulletIndices[0]), GL_UNSIGNED_INT, 0);*/


	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glFlush();
}

int InitializeNetwork(addrinfo* result)
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	// Resolve the server address and port
	addrinfo hints = { 0 };
	hints.ai_family = AF_UNSPEC; //AF_UNSPEC = IPv4 or IPv6, AF_INET = IPv4, AF_INET6 = IPv6
	hints.ai_socktype = SOCK_DGRAM; // SOCK_STREAM = TCP, SOCK_DGRAM = UDP
	hints.ai_protocol = IPPROTO_UDP; // IPPROTO_TCP = TCP, IPPROTO_UDP = UDP

	addrinfo* tempresult;
	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &tempresult);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	connectSocket = INVALID_SOCKET;
	for (addrinfo * ptr = tempresult; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		connectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (connectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}
		*result = *ptr;
		break;
	}

	if (connectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	u_long mode = 1;
	iResult = ioctlsocket(connectSocket, FIONBIO, &mode);
	if (iResult == SOCKET_ERROR) {
		printf("non blocking error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(connectSocket);
		WSACleanup();
		return 1;
	}
	srand(time(0));
	int tempid = rand();
	if (tempid < 0)
	{
		networkID = (char)tempid * -1;
	}
	else
	{
		networkID = (char)tempid;
	}
	
	return 0;
}

int SendMessageToServer(SOCKET sockfd, sockaddr* server, ClientToServerMessage* msg)
{
	char sendbuf[BUFSIZE];

	SerializeForServer(msg, sendbuf);
	int len = sizeof(*server);
	int iResult = sendto(sockfd, sendbuf, BUFSIZE, 0, server, len);
	if (iResult == SOCKET_ERROR) {
		printf("sendto failed with error: %d\n", WSAGetLastError());
		int temp = WSAGetLastError();
		closesocket(sockfd);
		WSACleanup();
		system("pause");
		return iResult;
	}
	return iResult;
}

int RecieveMessageFromServer(SOCKET sockfd, sockaddr* server, ServerToClientMessage* msg, char* recvbuf)
{

	int len = sizeof(*server);

	int iResult = recvfrom(sockfd, recvbuf, BUFSIZE, 0, server, &len);
	int temp = WSAGetLastError();
	
	if (iResult == SOCKET_ERROR &&  WSAGetLastError() != WSAEWOULDBLOCK) {
		printf("recvfrom failed with error: %d\n", WSAGetLastError());
		closesocket(sockfd);
		WSACleanup();
		system("pause");
		return iResult;
	}
	else if (iResult > 0)
	{
		DeserializeFromServer(recvbuf, msg);
	}
	return iResult;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	LRESULT result = 0;
	switch (uMsg) {
	case WM_CLOSE: {
		OutputDebugString(L"WM_CLOSE");
		PostQuitMessage(0);
	} break;
	case WM_SIZE: {
		OutputDebugString(L"WM_SIZE");
		width = LOWORD(lParam);
		height = HIWORD(lParam);
		PostMessage(hwnd, WM_PAINT, 0, 0);
	}break;
	case WM_PAINT: {
		OutputDebugString(L"WM_PAINT ??");
		draw();
		PAINTSTRUCT paint;
		BeginPaint(hwnd, &paint);
		EndPaint(hwnd, &paint);
	} break;
	default: {
		result = DefWindowProc(hwnd, uMsg, wParam, lParam);
	} break;
	}
	return result;

}

void inputpredict()
{
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		if (currentgamestate.players[i].ID == networkID)
		{
			if (ctsmsg.input[LEFT])
			{
				currentgamestate.players[i].rotation += 0.01f;
			}
			if (ctsmsg.input[RIGHT])
			{
				currentgamestate.players[i].rotation -= 0.01f;

			}
			
			if (ctsmsg.input[UP])
			{
				currentgamestate.players[i].position = Vec3(currentgamestate.players[i].position.x + cos(currentgamestate.players[i].rotation) * 0.01f, currentgamestate.players[i].position.y + sin(currentgamestate.players[i].rotation) * 0.01f, currentgamestate.players[i].position.z);
			}
			if (ctsmsg.input[DOWN])
			{
				currentgamestate.players[i].position = Vec3(currentgamestate.players[i].position.x - cos(currentgamestate.players[i].rotation)* 0.01f, currentgamestate.players[i].position.y - sin(currentgamestate.players[i].rotation)* 0.01f, currentgamestate.players[i].position.z);
			}
			if (ctsmsg.input[SHOOT] && !currentgamestate.players[i].isBulletActive)
			{
				currentgamestate.players[i].isBulletActive = true;
				currentgamestate.players[i].bulletrotation = currentgamestate.players[i].rotation;
				currentgamestate.players[i].bulletPosition = currentgamestate.players[i].position;
			}
			if (currentgamestate.players[i].isBulletActive)
			{
				currentgamestate.players[i].bulletPosition = Vec3(currentgamestate.players[i].bulletPosition.x + cos(currentgamestate.players[i].bulletrotation)* 0.1f, currentgamestate.players[i].bulletPosition.y + sin(currentgamestate.players[i].bulletrotation)* 0.1f, currentgamestate.players[i].bulletPosition.z);
			
				if (currentgamestate.players[i].bulletPosition.x > 6.4f || currentgamestate.players[i].bulletPosition.x < -6.4f
					|| currentgamestate.players[i].bulletPosition.y > 6.4f || currentgamestate.players[i].bulletPosition.y < -6.4f)
				{
					currentgamestate.players[i].isBulletActive = false;
				}
			}
			
		}
	}
}

void interpolate()
{
	
	float tCurrent = totalms - lastgamestate.time;
	float tTotal = currentgamestate.time - lastgamestate.time;
	float t = tTotal / tCurrent  ;
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		currentgamestate.players[i].position = lerpvec3(lastgamestate.players[i].position, currentgamestate.players[i].position,t);
		currentgamestate.players[i].bulletPosition = lerpvec3(lastgamestate.players[i].bulletPosition, currentgamestate.players[i].bulletPosition, t);
		currentgamestate.players[i].rotation = lerp(lastgamestate.players[i].rotation, currentgamestate.players[i].rotation, t);
		currentgamestate.players[i].bulletrotation = lerp(lastgamestate.players[i].bulletrotation, currentgamestate.players[i].bulletrotation, t);

	}
}


void updateNetwork()
{
	//No ID == not connected -> try to connect;
	if (connected == false)
	{
		for (int i = 0; i < sizeof(stcmsg.gameState.players) / sizeof(stcmsg.gameState.players[0]); i++)
		{
			if (stcmsg.gameState.players[i].ID == networkID)
			{
				connected = true;
			}
		}

	}
	//If not connected try connecting
	if (connected == false)
	{
		ctsmsg.ID = networkID;
		ctsmsg.type = MSG_FROMCLIENT_REQUEST_TO_JOIN;
		SendMessageToServer(connectSocket, result.ai_addr, &ctsmsg);
		RecieveMessageFromServer(connectSocket, result.ai_addr, &stcmsg, recvbuff);
		clientcurrentframe = stcmsg.gameState.frameCount;
		currentgamestate = stcmsg.gameState;
		lastgamestate = currentgamestate;
		totalms = currentgamestate.time;
	}
	//if connected send input
	if (connected == true)
	{
		ctsmsg.type = MSG_FROMCLIENT_INPUT;
		ctsmsg.ID = networkID;

		RecieveMessageFromServer(connectSocket, result.ai_addr, &stcmsg, recvbuff);
		if (stcmsg.gameState.frameCount > lastgamestate.frameCount)
		{
			lastgamestate = currentgamestate;
			currentgamestate = stcmsg.gameState;
			interpolate();
			
			SendMessageToServer(connectSocket, result.ai_addr, &ctsmsg);
		}
		// Simulates the same thing as server does in update
		inputpredict();
		
	}

}
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	WNDCLASS windowClass = { 0 };
	windowClass.style = CS_OWNDC; // CS_HREDRAW | CS_VREDRAW
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = hInstance;
	windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	windowClass.lpszClassName = L"my window class";
	initTimerStuff();
	if (RegisterClass(&windowClass)) {

		HWND hWnd = CreateWindowEx(
			0,
			windowClass.lpszClassName,
			L"?UU Spelprogrammering 3",
			WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			0, 0, hInstance, 0);

		if (hWnd) {

			HDC hDC = GetDC(hWnd);
			PIXELFORMATDESCRIPTOR pixelFormatDescriptor = { 0 };
			pixelFormatDescriptor.nSize = sizeof(pixelFormatDescriptor);
			pixelFormatDescriptor.nVersion = 1;
			pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDescriptor.cColorBits = 32;

			int pixelformat = ChoosePixelFormat(hDC, &pixelFormatDescriptor);
			if (pixelformat != 0) {
				if (SetPixelFormat(hDC, pixelformat, &pixelFormatDescriptor)) {

					HGLRC openGLContext = wglCreateContext(hDC);
					wglMakeCurrent(hDC, openGLContext);

					if (Win32LoadOpenGLFunctions()) {
						initOpenGL();
						const char* version = (const char*)glGetString(GL_VERSION);
					}
					if (InitializeNetwork(&result) == 0)
					{
						isRunning = 1;
					}

					while (isRunning) {
						MSG msg;
						BOOL hasMessage = PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE);

						if (hasMessage) {
							switch (msg.message) {
							case WM_QUIT: {
								OutputDebugString(L"WM_QUIT");
								isRunning = 0;
							} break;
							case WM_KEYDOWN: {
								switch (msg.wParam) {
								case VK_ESCAPE: isRunning = 0; break;
								case VK_LEFT:ctsmsg.input[LEFT] = true; break;
								case VK_RIGHT:ctsmsg.input[RIGHT] = true; break;
								case VK_UP:ctsmsg.input[UP] = true; break;
								case VK_DOWN:ctsmsg.input[DOWN] = true; break;
								case VK_SPACE:ctsmsg.input[SHOOT] = true; break;
								}
							} break;
							case WM_KEYUP: {
								switch (msg.wParam) {
								case VK_LEFT:ctsmsg.input[LEFT] = false; break;
								case VK_RIGHT:ctsmsg.input[RIGHT] = false; break;
								case VK_UP:ctsmsg.input[UP] = false; break;
								case VK_DOWN:ctsmsg.input[DOWN] = false; break;
								case VK_SPACE:ctsmsg.input[SHOOT] = false; break;
								}

							} break;
							default: {
								TranslateMessage(&msg);
								DispatchMessage(&msg);
							}
							}
						}

						if (isRunning) {
							
							updateNetwork();
							draw();
							sleepyTime();
							clientcurrentframe++;
						}
					}
				}
				else MessageBox(NULL, L"SetPixelFormat() failed.", L"Error", MB_OK);
			}
			else MessageBox(NULL, L"ChoosePixelFormat() failed.", L"Error", MB_OK);
		}
		else MessageBox(NULL, L"CreateWindow() failed.", L"Error", MB_OK);
	}
	else MessageBox(NULL, L"RegisterClass() failed.", L"Error", MB_OK);
	return 0;
}


/*int running = 1;

int main(int argc, char **argv) {
GameLoop *Loop = new GameLoop();
Loop->Initialize();
while (running > 0)
{
running = Loop->Update();
}
delete(Loop);
Loop = nullptr;
}*/

